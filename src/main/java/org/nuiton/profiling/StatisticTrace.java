package org.nuiton.profiling;

/*
 * #%L
 * Nuiton Profiling
 * %%
 * Copyright (C) 2014 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

/**
 * Permet d'avoir des statistiques sur l'utilisation de NuitonTrace lui meme
 * pour voir l'impact qu'il a sur les performances
 *
 * @author poussin
 * @version $Revision$
 *
 * Last update: $Date$
 * by : $Author$
 */
public class StatisticTrace extends Statistic {

    public StatisticTrace() {
        super("trace");
    }

    @Override
    public String formatValue(long value) {
//        String result = Unit.format(value, Unit.Time.nano, 3);
        String result = String.valueOf(Unit.Time.nano.convertTo(Unit.Time.s, value));
        return result;
    }

}
