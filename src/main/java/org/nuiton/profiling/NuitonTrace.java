/*
 * #%L
 * Nuiton Profiling
 * %%
 * Copyright (C) 2006 - 2014 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.profiling;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintStream;
import java.io.Writer;
import java.lang.reflect.Method;
import org.apache.commons.io.IOUtils;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.AfterThrowing;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;

/**
 * Permet de tracer les appels aux methodes.
 *
 * <h2>Mise en place</h2>
 * 
 * Il faut mettre dans les dependances Maven le nuiton-profiling
 * <pre>
    $lt;dependency$gt;
      $lt;groupId$gt;org.nuiton$lt;/groupId$gt;
      $lt;artifactId$gt;nuiton-profiling$lt;/artifactId$gt;
      $lt;version$gt;2.7$lt;/version$gt;
    $lt;/dependency$gt;
 * </pre>
 *
 * Pour l'utiliser il faut définir un fichier XML aop.xml dans le repertoire
 * META-INF ou WEB-INF qui intercepte les methodes souhaiter.
 * <pre>
 * &lt;!DOCTYPE aspectj PUBLIC "-//AspectJ//DTD//EN" "http://www.eclipse.org/aspectj/dtd/aspectj.dtd"$gt;
$lt;aspectj$gt;
  $lt;weaver options="-verbose"$gt;
    $lt;exclude within="fr.insee..*CGLIB*"/$gt;
  $lt;/weaver$gt;
  $lt;aspects$gt;
    $lt;concrete-aspect name="org.nuiton.profiling.NuitonTraceAppAspect"
                     extends="org.nuiton.profiling.NuitonTrace"$gt;
      $lt;pointcut name="executeMethod"
                expression="
      execution(* org.nuiton..*(..))
   || execution(* org.chorem..*(..))
   || execution(* org.apache..*(..))
   || execution(* ognl..*(..))"/$gt;
    $lt;/concrete-aspect$gt;
  $lt;/aspects$gt;
$lt;/aspectj$gt;
 * </pre>
 * Ensuite il faut lancer la JVM avec l'option
 * <pre>
 * -javaagent:path/to/aspectjweaver-1.7.1.jar
 * </pre>
 * normalement il se trouve dans le repo maven dans le repertoire
 * <ul>
 * <li>$MAVEN_REPO/org/aspectj/aspectjweaver/1.7.1/aspectjweaver-1.7.1.jar</li>
 * </ul>
 *
 * <h2>Utilisation</h2>
 *
 * Il y a 3 modes: interactif, non interactif et par programmation
 *
 * <h3>Mode non interactif</h3>
 *
 * Il faut mettre le nom d'un fichier dans la variable d'environnnement
 * {@link #AUTO_SAVE_FILENAME_OPTION}. Dans ce cas lors de l'arrêt de la JVM
 * les statistiques sont ecrites dans ce fichier.
 *
 * <h3>Mode interactif</h3>
 *
 * Il est possible de demander a a NuitonTrace lorsqu'il s'initialise de demarrer
 * un serveur Web qui permet de voir et recuperer les donnees statistiques.
 * Pour cela il faut indique sur quel port le serveur doit attendre via la
 * variable d'environnnement {@link #PORT_OPTION}
 *
 * <h3>Par programmation</h3>
 * 
 * Pour afficher les statistiques dans votre programme
 * <ul>
 * <li> log.info(NuitonTrace.getStatisticsAndClear());</li>
 * <li> NuitonTrace.printStatistiqueAndClear();</li>
 * </ul>
 *
 * <h2>Autre mode de fonctionnement</h2>
 *
 * Il doit être possible, plutot que d'écrire un fichier XML, de sous classer
 * NuitonTrace en ajoutant par exemple
 * 
 * <pre>
 * \@Expression( "execution(* fr.ifremer.isisfish.datastore.ResultStorage.*(..))" +
 * "|| execution(* fr.ifremer.isisfish.aspect.Cache.*(..))" +
 * "|| execution(* fr.ifremer.isisfish.aspect.Trace.*(..))" +
 * "|| execution(* org.nuiton.topia..*(..))" +
 * "|| execution(* org.nuiton.math.matrix..*(..))" +
 * "|| execution(* fr.ifremer.isisfish.types..*(..))" +
 * "|| execution(* org.apache.commons.collections..*(..))"
 * )
 * Pointcut executeMethod;
 * </pre>
 *
 * @author bpoussin : poussin@codelutin.com
 * @author tchemit : chemit@codelutin.com
 */
@Aspect
public abstract class NuitonTrace {

    ///////////////////////////////////////////////////////////////////////////
    //
    // A S P E C T
    //
    ///////////////////////////////////////////////////////////////////////////

    /** statistique de tous les appels */
    public static Trace trace;

    public NuitonTrace() {
    }

    // abstract pointcut: no expression is defined
    @Pointcut
    abstract void executeMethod();

    @Before("executeMethod() && !within(org.nuiton.profiling.NuitonTrace.*)")
    public void traceBeforeExecute(JoinPoint jp) {
        // ajout dans le stack
        Method method = ((MethodSignature) jp.getSignature()).getMethod();

        NuitonTrace.trace.enter(method);
    }

    @AfterThrowing("executeMethod() && !within(org.nuiton.profiling.NuitonTrace.*)")
    public void traceAfterThrowingExecute(JoinPoint jp) {
        // si une exeption est leve, il faut faire la meme chose
        traceAfterExecute(jp);
    }

    @After("executeMethod() && !within(org.nuiton.profiling.NuitonTrace.*)")
    public void traceAfterExecute(JoinPoint jp) {
        Method method = ((MethodSignature) jp.getSignature()).getMethod();
        NuitonTrace.trace.exit(method);
    }


    ///////////////////////////////////////////////////////////////////////////
    //
    // P U B L I S H
    //
    ///////////////////////////////////////////////////////////////////////////


    /** @return les statistiques in CSV format*/
    static public String getStatisticsCSVAndClear() {
        String result  =trace.getStatisticsCSV();
        trace.clearStatistics();

        return result;
    }

    /**
     * @return les statistiques in json format
     * @since 2.7
     */
    static public String getStatisticsJson() {
        String result = trace.getStatisticsJson();
        return result;
    }

    /**
     * @since 2.7
     */
    static public void clearStatistics() {
        trace.clearStatistics();
    }

    /** @return les statistiques */
    static public String getStatisticsAndClear() {
        String result = trace.getStatisticsText();
        trace.clearStatistics();
        return result;
    }

    static public void printStatisticsAndClear() {
        printStatisticsAndClear(System.out);
    }

    static public void printStatisticsAndClear(PrintStream printer) {
        String stat = getStatisticsAndClear();
        if (stat != null && !"".equals(stat)) {
            printer.println(stat);
        }
    }


    ///////////////////////////////////////////////////////////////////////////
    //
    // M A I N   A N D   I N I T
    //
    ///////////////////////////////////////////////////////////////////////////


    /**
     * constante determinant le nom de la variable d'environnement a lire
     * pour savoir si les statistiques des threads doivent etre melange ou non
     * @since 2.7.2
     */ // ne doit pas contenir de '.' ou de '-' sinon elle est non utiliable en variable d'env
    final static public String DISTINCT_THREAD_CALL_OPTION = "nuitonprofiling_distinctThreadCall";
    /**
     * constante determinant le nom de la variable d'environnement a lire
     * pour connaitre le port a utiliser pour le serveur web
     * @since 2.7
     */ // ne doit pas contenir de '.' ou de '-' sinon elle est non utiliable en variable d'env
    final static public String PORT_OPTION = "nuitonprofiling_webport";
    /**
     * constante determinant le nom de la variable d'environnement a lire
     * pour connaitre le nom du fichier de sauvegarde a la sortie de la JVM
     * @since 2.7
     */ // ne doit pas contenir de '.' ou de '-' sinon elle est non utiliable en variable d'env
    final static public String AUTO_SAVE_FILENAME_OPTION = "nuitonprofiling_autosavefile";
    /**
     * Indique si nuiton trace a deja ete initialise ou non
     * @since 2.7
     */
    static protected boolean initilized = false;

    /**
     * Permet de lire les options de la ligne de commande Java (-D) et si non
     * trouver de rechercher dans les variables d'environnement
     * @param optionName
     * @return la valeur de l'option ou null si non trouve
     * @since 2.7
     */
    static protected String getOption(String optionName) {
        String result = System.getProperty(optionName);
        if (result == null) {
            result = System.getenv(optionName);
        }
        return result;
    }

    /**
     * Recherche la configuration dans les variables d'environnement et les
     * utilise pour mettre en place ce qui est demande par l'utilisateur
     * @since 2.7
     */
    static protected void init() {
        if (initilized) {
            return;
        }
        initilized = true;
        
        System.out.println("Init Nuiton Profiling ...");

        String distinctThreadCall = getOption(DISTINCT_THREAD_CALL_OPTION);
        trace = new Trace(true, Boolean.parseBoolean(distinctThreadCall));

        String portString = getOption(PORT_OPTION);
        System.out.println("NuitonProfiling web port: " + portString);
        if (portString != null) {
            try {
                int port = Integer.parseInt(portString);
                if (port > 0) {
                    new WebService(port).start();
                }
            } catch (Exception eee) {
                eee.printStackTrace(System.err);
                System.err.println("Can't parse port number: "+ portString);
            }
        }

        final String file = getOption(AUTO_SAVE_FILENAME_OPTION);
        System.out.println("NuitonProfiling auto save file: " + file);
        if (file != null) {
            Runtime.getRuntime().addShutdownHook(new Thread() {
                @Override
                public void run() {
                    Writer out = null;
                    try {
                        String stat = NuitonTrace.getStatisticsCSVAndClear();
                        out = new FileWriter(file);
                        out.write(stat);
                    } catch (IOException eee) {
                        eee.printStackTrace(System.err);
                        System.err.println("Can't write Statistic file: " + file);
                    } finally {
                        IOUtils.closeQuietly(out);
                    }
                }
            });
        }
    }

    /**
     * Constructeur static pour initialise nuiton trace
     * @since 2.7
     */
    static {
        // attention aucune classe utilisee dans le init ne doit utiliser
        // des classes surveille par NuitonTrace, sinon y'a un probleme d'init
        // l'AOP voulant surveillee une method, mais l'AOP n'est pas encore
        // initialise puisqu'on est dans l'init :(
        init();
    }

    /**
     * Force le lancement du serveur web, permet d'avoir le serveur sur une
     * machine pour monitorer une application qui est sur une autre. L'avantage
     * est de pouvoir centraliser les sauvegardes sur le meme domaine web
     * @param args le port a utiliser si vide utilise les variables d'environnement
     * et en dernier recourrs utilise le port 4488
     */
    static public void main(String... args) {
        System.out.println("Starting Nuiton Profiling ...");
        String portString;
        if (args != null && args.length > 0) {
            portString = args[0];
        } else {
            portString = getOption(PORT_OPTION);
        }
        if (portString == null) {
            portString = "4488";
        }
        System.out.println("NuitonProfiling web port: " + portString);
        try {
            int port = Integer.parseInt(portString);
            if (port > 0) {
                new WebService(port).start();
            }
        } catch (Exception eee) {
            eee.printStackTrace(System.err);
            System.err.println("Can't parse port number: "+ portString);
        }
    }
}
