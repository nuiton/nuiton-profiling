package org.nuiton.profiling;

/*
 * #%L
 * Nuiton Profiling
 * %%
 * Copyright (C) 2014 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import java.lang.reflect.Method;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Statistique sur un appele de methode
 */
public class StatisticMethod extends Statistic {

    String threadName;

    Method method;
    /** nombre d'appel vers une autre method depuis cette methode */
    long callNestMethod;
    /** temps total d'execution de cette methode (avec le temps des autres methodes) */
    long totalNestMethod;
    Counter<Method> callers = new Counter<Method>();

    public StatisticMethod(Method method) {
        super(method.toString());
        this.method = method;
    }

    public StatisticMethod(String threadName, Method method) {
        this(method);
        this.threadName = threadName;
    }

    /**
     *
     *
     * @param caller
     * @param time time spent in this method in nonasecond
     * @param nestTime
     * @param nestCall
     */
    public void add(Method caller, long time, long nestTime, long nestCall) {
        super.add(time);
        callers.add(caller); // cout ~400ms pour 2M d'appel
        callNestMethod += nestCall;
        totalNestMethod += nestTime;
    }

    @Override
    public String formatValue(long value) {
//        String result = Unit.format(value, Unit.Time.nano, Unit.Time.s, 3);
        String result = String.valueOf(Unit.Time.nano.convertTo(Unit.Time.s, value));
        return result;
    }

    @Override
    public StringBuilder exportCSV(StringBuilder result) {
        if (threadName != null) {
            if (result == null) {
                result = new StringBuilder();
            }
            result.append(threadName).append(";");
        }
        result = super.exportCSV(result);
        result.append(";").append(callNestMethod);
        result.append(";").append(formatValue(totalNestMethod));
        result.append(";").append(callers);
        return result;
    }

    @Override
    public StringBuilder exportJSON(StringBuilder result) {
        result = super.exportJSON(result);
        result.deleteCharAt(result.length() - 1); // remove last '}'
        if (threadName != null) {
            result.append(",").append("'thread':").append("'").append(threadName).append("'");
        }
        result.append(",").append("'method':").append("'").append(method).append("'").append(",");
        result.append("'call_nest':").append(callNestMethod).append(",");
        result.append("'total_with_nest':").append(formatValue(totalNestMethod));
        result.append(",").append("'callers': [");
        for (Map.Entry<Method, AtomicInteger> c : callers) {
            result.append("{").append("'caller':").append("'").append(c.getKey()).append("'").append(",").append("'call':").append(c.getValue()).append("},");
        }
        result.append("]}");
        return result;
    }

    @Override
    public StringBuilder exportText(StringBuilder result) {
        if (threadName != null) {
            if (result == null) {
                result = new StringBuilder();
            }
            result.append("[").append(threadName).append("] ");
        }
        result = super.exportText(result);
        result.append(" call_nest: ").append(callNestMethod).append(" total_with_nest: ").append(formatValue(totalNestMethod));
        result.append(" callers: ").append(callers);
        return result;
    }

}
