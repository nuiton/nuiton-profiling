package org.nuiton.profiling;

/*
 * #%L
 * Nuiton Profiling
 * %%
 * Copyright (C) 2014 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Classe qui permet d'ajouter un element et compte le nombre de fois
 * que l'on a ajoute cet element.
 *
 * exemple d'utilisation: on souhaite compter le nombre de fois qu'une methode
 * est appele par une autre, on ajoute a chaque fois que quelqu'un fait
 * appele a elle ce quelqu'un.
 *
 * @param <E> le type d'element a compter
 */
public class Counter<E> implements Iterable<Map.Entry<E, AtomicInteger>> {

    protected Map<E, AtomicInteger> data = new HashMap<E, AtomicInteger>();

    /**
     * indique que l'on ajoute un element
     * @param e
     */
    public void add(E e) {
        AtomicInteger v = data.get(e);
        if (v == null) {
            v = new AtomicInteger();
            data.put(e, v);
        }
        v.incrementAndGet();
    }

    /**
     * Retourne la liste des objets ajouter avec le nombre de fois qu'ils
     * ont ete ajoute. Trie par le nombre de fois qu'ils ont ete ajoute,le
     * plus grand nombre en 1er
     *
     * @return
     */
    @Override
    public String toString() {
        StringBuilder result = new StringBuilder();
        for (Map.Entry<E, AtomicInteger> e : this) {
            result.append(e.getValue()).append("=[").append(e.getKey()).append("]").append(",");
        }
        if (result.length() > 0) {
            // on supprime la derniere virgule en trop
            result.deleteCharAt(result.length() - 1);
        }
        return result.toString();
    }

    @Override
    public Iterator<Map.Entry<E, AtomicInteger>> iterator() {
        Comparator<Map.Entry<E, AtomicInteger>> comparator = new Comparator<Map.Entry<E, AtomicInteger>>() {
            @Override
            public int compare(Map.Entry<E, AtomicInteger> o1, Map.Entry<E, AtomicInteger> o2) {
                // on inverse, car on veut le plus grand en 1er
                return Integer.compare(o2.getValue().get(), o1.getValue().get());
            }
        };
        Set<Map.Entry<E, AtomicInteger>> entries = data.entrySet();
        Set<Map.Entry<E, AtomicInteger>> sorted = new TreeSet<Map.Entry<E, AtomicInteger>>(comparator);
        sorted.addAll(entries);
        return sorted.iterator();
    }

}
