package org.nuiton.profiling;

/*
 * #%L
 * Nuiton Profiling
 * %%
 * Copyright (C) 2014 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import com.sun.net.httpserver.Headers;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import com.sun.net.httpserver.HttpServer;
import com.sun.net.httpserver.spi.HttpServerProvider;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.InetSocketAddress;
import java.net.URI;
import java.net.URL;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;

import static org.nuiton.profiling.NuitonTrace.clearStatistics;
import static org.nuiton.profiling.NuitonTrace.trace;

/**
 *
 * @author poussin
 * @version $Revision$
 *
 * Last update: $Date$
 * by : $Author$
 */
public class WebService {

    /**
     * On conserve les stats sur les differentes demandes
     */
    static protected StatisticMemory memoryStat = new StatisticMemory();
    
    protected InetSocketAddress inet;
    protected HttpServer server;

    /**
     * Demarre le service web sur le port demande
     * @param port un port valide sur lequel attendre
     * @since 2.7
     */
    public WebService(int port) {
        try {
            HttpServerProvider provider = HttpServerProvider.provider();
            inet = new InetSocketAddress(port);
            server = provider.createHttpServer(inet, 0);

            server.createContext("/", new WebHandler());
            server.createContext("/data/json", new DataJSONHandler());
            server.createContext("/data/csv", new DataCSVHandler());
            server.createContext("/api", new ApiHandler());

        } catch(Throwable eee) {
            System.err.println("Can't start web service on port " + port);
            eee.printStackTrace(System.err);
        }
    }

    public void start() {
        System.out.println("Nuiton profiling Server starting...");
        server.start();
        System.out.println("Nuiton profiling Server is listening on " + inet );
    }

    /**
     * Handler pour le serveur web qui gere le demande de statistique
     * @since 2.7
     */
    static class WebHandler implements HttpHandler {

        static final public String filepath = "/org/nuiton/profiling/web/";

        @Override
        public void handle(HttpExchange exchange) throws IOException {
            String requestMethod = exchange.getRequestMethod();
            if (requestMethod.equalsIgnoreCase("GET")) {
                String path = exchange.getRequestURI().getPath();
                String file = StringUtils.substringAfterLast(path, "/");
                file = StringUtils.defaultIfBlank(file, "index.html");
                URL resource = getClass().getResource(filepath + file);
                if (resource == null) {
                    exchange.sendResponseHeaders(404, 0);
                    exchange.close();
                } else {
                    String ext = StringUtils.substringAfterLast(file, ".");
                    String type;
                    if (StringUtils.equalsIgnoreCase(ext, "mf")) {
                        type = "text/cache-manifest";
                    } else if (StringUtils.equalsIgnoreCase(ext, "ico")) {
                        type = "image/x-icon";
                    } else if (StringUtils.equalsIgnoreCase(ext, "js")) {
                        type = "text/javascript";
                    } else {
                        // html, css, js
                        type = "text/" + ext;
                    }
                    Headers responseHeaders = exchange.getResponseHeaders();
                    responseHeaders.set("Content-Type", type);
                    exchange.sendResponseHeaders(200, 0);

                    InputStream in = resource.openStream();
                    ByteArrayOutputStream content = new ByteArrayOutputStream();
                    IOUtils.copy(in, content);

                    OutputStream responseBody = exchange.getResponseBody();
                    responseBody.write(content.toByteArray());
                    responseBody.close();
                    IOUtils.closeQuietly(in);
                    IOUtils.closeQuietly(content);
                }
            }

        }
    }

    /**
     * Handler pour le serveur web qui gere le demande de statistique
     * @since 2.7
     */
    static class DataJSONHandler implements HttpHandler {
        /**
         * Permet de recupere le parametre callback de l'URL, utilise pour le jsonp
         * @param exchange
         * @return
         */
        protected String getCallback(HttpExchange exchange) {
            String result = null;
            URI uri = exchange.getRequestURI();
            String query = uri.getQuery();
            if (query != null) {
                String pairs[] = query.split("[&]");

                for (String pair : pairs) {
                    String param[] = pair.split("[=]", 2);
                    if (param.length == 2 && "callback".equalsIgnoreCase(param[0])) {
                        result = param[1];
                        break;
                    }
                 }
            }
            return result;
        }

        @Override
        public void handle(HttpExchange exchange) throws IOException {
            String requestMethod = exchange.getRequestMethod();
            if (requestMethod.equalsIgnoreCase("GET")) {
                Headers responseHeaders = exchange.getResponseHeaders();
                responseHeaders.set("Content-Type", "text/javascript");
                exchange.sendResponseHeaders(200, 0);

                String jsoncallback = getCallback(exchange);
                String json;

                String path = exchange.getRequestURI().getPath();
                if (StringUtils.endsWithIgnoreCase(path, "memory")) {
                    memoryStat.add(Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory());
                    json = "{" + memoryStat.exportJSON(null) + "}";
                } else {
                    json = trace.getStatisticsJson();
                }

                if (jsoncallback != null) {
                    json = jsoncallback + "(" + json + ")";
                }
                OutputStream responseBody = exchange.getResponseBody();
                responseBody.write(json.getBytes());
                responseBody.close();
            }
        }
    }

    /**
     * Handler pour le serveur web qui gere le demande de statistique
     * @since 2.7
     */
    static class DataCSVHandler implements HttpHandler {

        @Override
        public void handle(HttpExchange exchange) throws IOException {
            String requestMethod = exchange.getRequestMethod();
            if (requestMethod.equalsIgnoreCase("GET")) {
                Headers responseHeaders = exchange.getResponseHeaders();
                responseHeaders.set("Content-Type", "text/plain");
                exchange.sendResponseHeaders(200, 0);

                String csv;

                String path = exchange.getRequestURI().getPath();
                if (StringUtils.endsWithIgnoreCase(path, "memory")) {
                    memoryStat.add(Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory());
                    csv = memoryStat.exportCSV(null).toString();
                } else {
                    csv = trace.getStatisticsCSV();
                }

                OutputStream responseBody = exchange.getResponseBody();
                responseBody.write(csv.getBytes());
                responseBody.close();
            }
        }
    }
    /**
     * Handler pour le serveur web qui gere l'api:
     * <li>clear
     * @since 2.7
     */
    static class ApiHandler implements HttpHandler {

        @Override
        public void handle(HttpExchange exchange) throws IOException {
            String requestMethod = exchange.getRequestMethod();
            if (requestMethod.equalsIgnoreCase("GET")) {
                String path = exchange.getRequestURI().getPath();
                if (StringUtils.endsWithIgnoreCase(path, "clear")) {
                    Headers responseHeaders = exchange.getResponseHeaders();
                    responseHeaders.set("Content-Type", "text/plain");
                    exchange.sendResponseHeaders(200, 0);

                    clearStatistics();

                    OutputStream responseBody = exchange.getResponseBody();
                    responseBody.write("ok".getBytes());
                    responseBody.close();
                } else if (StringUtils.endsWithIgnoreCase(path, "gc")) {
                    Headers responseHeaders = exchange.getResponseHeaders();
                    responseHeaders.set("Content-Type", "text/plain");
                    exchange.sendResponseHeaders(200, 0);

                    Runtime.getRuntime().gc();

                    OutputStream responseBody = exchange.getResponseBody();
                    responseBody.write("ok".getBytes());
                    responseBody.close();
                } else {
                    exchange.sendResponseHeaders(404, 0);
                    exchange.close();
                }
            }

        }
    }

}
