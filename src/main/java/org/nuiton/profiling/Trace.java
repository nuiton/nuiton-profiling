/*
 * #%L
 * Nuiton Profiling
 * %%
 * Copyright (C) 2006 - 2014 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.profiling;

import java.io.PrintStream;
import java.lang.reflect.Method;
import java.util.ArrayDeque;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentLinkedDeque;

/**
 * Permet de tracer les appels aux methodes.
 * 
 * @author bpoussin : poussin@codelutin.com
 * @author tchemit : chemit@codelutin.com
 */
public class Trace {

    static protected class StatAndStack {
        protected String threadName;

        /** statistic on method call */
        protected Map<Method, StatisticMethod> statistics;

        /** variable used when Trace instanciated in monothread mode */
        protected ArrayDeque<Call> stack;

        public StatAndStack(String threadName, 
                Map<Method, StatisticMethod> statistics, ArrayDeque<Call> stack) {
            this.threadName = threadName;
            this.statistics = statistics;
            this.stack = stack;
        }

        /**
         * Retourne les statistiques d'appele pour la methode passee en parametre
         *
         * @param method
         * @return
         */
        public StatisticMethod getStatistics(Method method) {
            StatisticMethod result = statistics.get(method);
            if (result == null) {
                result = new StatisticMethod(threadName, method);
                statistics.put(method, result);
            }
            return result;
        }

        public Map<Method, StatisticMethod> getStatistics() {
            return statistics;
        }
    }

    protected boolean distinctThreadCall = false;
    protected boolean multithread = true;

    /** statistic on method call */
    protected Map<Method, StatisticMethod> commonStatistics;

    protected StatAndStack monoStatAndStack;
    
    protected ConcurrentLinkedDeque<StatAndStack> allStatAndStack =
            new ConcurrentLinkedDeque<StatAndStack>();

    /**
     * array : [nest method call, start time, start time with child]
     *
     * On ne melange pas les stack entre les threads, sinon les resultats
     * ne veulent plus rien dire car toutes les methodes des threads sont
     * melangees
     */
    protected static final ThreadLocal<StatAndStack> callStacks = new ThreadLocal<StatAndStack> ();

    /**
     * default usable in multi-thread context, all calls in all threads
     * are stored in same place (no thread distinction)
     */
    public Trace() {
    }

    /**
     * indique que l'on utilise Trace seulement avec un thread, cela evite
     * d'utiliser en interne un ThreadLocal
     * @param multithread si vrai indique que cette instance est utilise dans un context
     * multithread. Dans ce cas il est possible d'indique la politique de
     * collect des appels via le parametre distinctThreadCall
     * @param distinctThreadCall si vrai indique que les appels des differents thread
     * ne doivent pas etre melange. Si deux thread appel la meme methode
     * cette methode se trouvera dans des statistiques  differentes pour chacun des threads
     */
    public Trace(boolean multithread, boolean distinctThreadCall) {
        this.multithread = multithread;
        this.distinctThreadCall = distinctThreadCall;
    }

    public StatAndStack getMonoStatAndStack() {
        if (monoStatAndStack == null) {
            monoStatAndStack = new StatAndStack(
                    Thread.currentThread().getName(),
                    new ConcurrentHashMap<Method, StatisticMethod>(),
                    new ArrayDeque<Call>());
            allStatAndStack.add(monoStatAndStack);
        }
        return monoStatAndStack;
    }

    protected Map<Method, StatisticMethod> getCommonStatistics() {
        if (commonStatistics == null) {
            commonStatistics = new ConcurrentHashMap<Method, StatisticMethod>();
        }
        return commonStatistics;
    }

    public StatAndStack getStack() {
        StatAndStack result;
        if (!multithread) {
            result = getMonoStatAndStack();
        } else {
            result = callStacks.get();
            if (result == null) {
                // initialisation for current thread
                String threadName;
                Map<Method, StatisticMethod> stats;
                ArrayDeque<Call> stack = new ArrayDeque<Call>();

                if (distinctThreadCall) {
                    threadName = Thread.currentThread().getName();
                    stats = new ConcurrentHashMap<Method, StatisticMethod>();
                } else {
                    threadName = "ALL";
                    stats = getCommonStatistics();
                }

                result = new StatAndStack(threadName, stats, stack);
                callStacks.set(result);
                allStatAndStack.add(result);
            }
        }

        return result;
    }

    public void enter(Method method) {
        long current = System.nanoTime();

        Call stackItem = new Call(method, 0, current, current);
        getStack().stack.push(stackItem);
    }

    public void exit(Method method) {
        long current = System.nanoTime();

        // trop couteux a de le faire a chaque appel (2s pour 2M d'appel)
//        memoryStat.add(Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory());

        StatAndStack ss = getStack();

        ArrayDeque<Call> callStack = ss.stack;
        if (callStack.isEmpty()) {
            // bad state do nothing
        } else {
            Call stackItem = callStack.pop();
            long timeSpent = current - stackItem.timeStart;
            long timeSpentNestMethod = current - stackItem.timeStartNestMethod;

            Method caller = null;
            if (!callStack.isEmpty()) {
                Call parent = callStack.peek();
                parent.add(timeSpentNestMethod);

                caller = parent.method;
            }

            StatisticMethod stat = ss.getStatistics(method);
            stat.add(caller, timeSpent, timeSpentNestMethod, stackItem.callNestMethod);

        }

        // temps passer pour nuiton trace lui meme
        // trop couteux a de le faire de stat sur les stats (2s pour 2M d'appel)
//        long traceTime = System.nanoTime() - current;
//        traceStat.add(traceTime);

        // ajouter le delta de temps dans le temps passé dans la méthod

        // il faud garder le temps passé dans l'appel d'autre methode de la stack
        // --> A
        //   =========
        //   --> B
        //   <-- B
        //   =========
        //   --> C
        //     --> D
        //     <-- D
        //   <-- C
        //   =========
        // <-- A

        // le temps reellement passé dans A est representé par les =====
    }

    /** @return les statistiques in CSV format*/
    public String getStatisticsCSV() {
        StringBuilder result = new StringBuilder();
        result
                .append("thread;")
                .append("method;")
                .append("call;")
                .append("min;")
                .append("mean;")
                .append("max;")
                .append("total;")
                .append("deviation;")
                .append("slope;")
                .append("call_nest;")
                .append("total_with_nest;")
                .append("callers;")
                .append("\n");

        for (StatAndStack ss : allStatAndStack) {
            for (StatisticMethod stat : ss.statistics.values()) {
                stat.exportCSV(result);
                result.append("\n");
            }
        }

        return result.toString();
    }

    /** 
     * @return les statistiques in json format
     * @since 2.7
     */
    public String getStatisticsJson() {
        StringBuilder result = new StringBuilder();
        result.append("{");

        String separator = "";
        
        for (StatAndStack ss : allStatAndStack) {
            for (StatisticMethod stat : ss.statistics.values()) {

                result.append(separator);
                separator = ","; // separtor est vide seulement la 1ere fois

                stat.exportJSON(result);

                result.append("\n");
            }
        }
        result.append("}");
        return result.toString();
    }

    /**
     * Clear all statistiques.
     * 
     * @since 2.7
     */
    public void clearStatistics() {
        for (StatAndStack ss : allStatAndStack) {
            ss.statistics.clear();
        }
    }

    /** @return les statistiques */
    public String getStatisticsText() {
        StringBuilder result = new StringBuilder();
        result.append("--- Statistics ---\n");
        for (StatAndStack ss : allStatAndStack) {
            for (StatisticMethod stat : ss.statistics.values()) {

                stat.exportText(result);
                result.append("\n");
            }
        }
        result.append("--------------------\n");
        return result.toString();
    }

    public void printStatisticsAndClear() {
        printStatisticsAndClear(System.out);
    }

    public void printStatisticsAndClear(PrintStream printer) {
        String stat = getStatisticsText();
        if (stat != null && !"".equals(stat)) {
            clearStatistics();
            printer.println(stat);
        }
    }

    /**
     * Represente un call de methode
     */
    protected static class Call {
        Method method;
        /** nombre d'appel vers une autre method depuis cette methode */
        long callNestMethod;
        /** heure de depart de l'appel a la methode (sans le temps des autres methodes) */
        long timeStart;
        /** heure de depart de l'appel a la methode (avec le temps des autres methodes) */
        long timeStartNestMethod;

        public Call(Method method, long callNestMethod, long timeStart, long timeStartNestMethod) {
            this.method = method;
            this.callNestMethod = callNestMethod;
            this.timeStart = timeStart;
            this.timeStartNestMethod = timeStartNestMethod;
        }

        public void set(Method method, long callNestMethod, long timeStart, long timeStartNestMethod) {
            this.method = method;
            this.callNestMethod = callNestMethod;
            this.timeStart = timeStart;
            this.timeStartNestMethod = timeStartNestMethod;
        }

        public void add(long time) {
            callNestMethod++;  // add +1 to call number nest method
            timeStart += time; // remove to time all time spent in nest method (yes + to remove :)
        }

    }

}
