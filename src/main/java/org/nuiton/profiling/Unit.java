package org.nuiton.profiling;

/*
 * #%L
 * Nuiton Profiling
 * %%
 * Copyright (C) 2014 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

/**
 * Helper class to format number. We don't use existing format method in other
 * library to minimise dependency. NuitonTrace can't work correctly if he has
 * external dependncy.
 *
 * @author poussin
 * @version $Revision$
 *
 * Last update: $Date$
 * by : $Author$
 */
public class Unit {

    static public enum Memory {
        o(1024), Ko(1024), Mo(1024), Go(1024), To(1);

        final private double factor;

        private Memory(double f) {
            this.factor = f;
        }

        Memory next() {
            Memory[] values = Memory.values();
            Memory result = null;
            int next = this.ordinal()+1;
            if (next < values.length) {
                result = values[next];
            }
            return result;
        }

        Memory prev() {
            Memory[] values = Memory.values();
            Memory result = null;
            int prev = this.ordinal()-1;
            if (prev >= 0) {
                result = values[prev];
            }
            return result;
        }

        double convertTo(Memory target, double value) {
            double result = value;
            if (this.compareTo(target) < 0) {
                Memory u = this;
                while ( u != target ) {
                    result = result / u.factor;
                    u = u.next();
                }
            } else if (this.compareTo(target) > 0){
                Memory u = this;
                while ( u != target ) {
                    u = u.prev();
                    result = result * u.factor;
                }
            }
            return result;
        }
    }

    static public enum Time {
        nano(1000000), milli(1000), s(60), m(60), h(24), d(1);

        final private double factor;

        private Time(double f) {
            this.factor = f;
        }

        public Time next() {
            Time[] values = Time.values();
            Time result = null;
            int next = this.ordinal()+1;
            if (next < values.length) {
                result = values[next];
            }
            return result;
        }

        public Time prev() {
            Time[] values = Time.values();
            Time result = null;
            int prev = this.ordinal()-1;
            if (prev >= 0) {
                result = values[prev];
            }
            return result;
        }

        public double convertTo(Time target, double value) {
            double result = value;
            if (this.compareTo(target) < 0) {
                Time u = this;
                while ( u != target ) {
                    result = result / u.factor;
                    u = u.next();
                }
            } else if (this.compareTo(target) > 0){
                Time u = this;
                while ( u != target ) {
                    u = u.prev();
                    result = result * u.factor;
                }
            }
            return result;
        }
    }

    static public String format(double value, Memory source, Memory target, int precision) {
        double v = source.convertTo(target, value);
        String result = String.format("%,."+precision+"f", v);
        result += target.toString();
        return result;
    }

    static public String format(double value, Time source, Time target, int precision) {
        double v = source.convertTo(target, value);
        String result = String.format("%,."+precision+"f", v);
        result += target.toString();
        return result;
    }

    /**
     * Choice better target unit for this value. source unit must be small unit
     * like octet or millis or nanosecond.
     * 
     * @param value
     * @param source
     * @param precision
     * @return 
     */
    static public String format(double value, Memory source, int precision) {
        Memory u = source;
        Memory target = source;

        double sign = value == 0 ? 1 : value / Math.abs(value);
        double tmp = Math.abs(value);
        while (u != null && tmp > u.factor) {
            tmp = tmp / u.factor;
            target = u;
            u = u.next();
        }
        if (u != null) {
            target = u;
        }

        u = target.prev();
        while (u != null && tmp < 1) {
            tmp = tmp * u.factor;
            target = u;
            u = u.prev();
        }

        tmp *= sign;
        String result = String.format("%,."+precision+"f", tmp);
        result += target.toString();

        return result;
    }

    /**
     * Choice better target unit for this value. source unit must be small unit
     * like octet or millis or nanosecond.
     *
     * @param value
     * @param source
     * @param precision
     * @return
     */
    static public String format(double value, Time source, int precision) {
        Time u = source;
        Time target = source;

        double sign = value == 0 ? 1 : value / Math.abs(value);
        double tmp = Math.abs(value);
        while (u != null && tmp > u.factor) {
            tmp = tmp / u.factor;
            target = u;
            u = u.next();
        }
        if (u != null) {
            target = u;
        }

        u = target.prev();
        while (u != null && tmp < 1) {
            tmp = tmp * u.factor;
            target = u;
            u = u.prev();
        }

        tmp *= sign;
        String result = String.format("%,."+precision+"f", tmp);
        result += target.toString();

        return result;
    }
}
