package org.nuiton.profiling;

/*
 * #%L
 * Nuiton Profiling
 * %%
 * Copyright (C) 2014 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

/**
 * Statistique sur un appele de methode
 */
public class Statistic {

    String name;
    /** nombre d'appel */
    long call;
    /** temps mini d'execution de cette methode (sans le temps des autres methodes) */
    long min = Long.MAX_VALUE;
    /** temps max d'execution de cette methode (sans le temps des autres methodes) */
    long max;
    /** temps total d'execution de cette methode (sans le temps des autres methodes) */
    long total;
    // for variance
    double delta = 0;
    double mean = 0;
    double M2 = 0;
    // for Linear regression
    double lrTimeCall = 0;

    public Statistic(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void add(long value) {
        call++;
        total += value;
        if (min > value) {
            min = value;
        }
        if (max < value) {
            max = value;
        }
        // compute linear regression
        lrTimeCall += value * call;
        // compute variance
        delta = value - mean;
        mean += delta / call;
        M2 += delta * (value - mean);
    }

    public long getCall() {
        return call;
    }

    public long getTotal() {
        return total;
    }

    public long getMax() {
        return max;
    }

    public long getMin() {
        return min;
    }

    public double getMean() {
        double result = Double.NaN;
        try {
            result = total / call;
        } catch (Exception eee) {
            // probably call = 0 -> division by 0
        }
        return result;
    }

    /**
     * Compute Standard deviation (ecart type)
     * @return
     */
    public double getStandardDeviation() {
        double variance = M2 / Math.max(1, call - 1);
        double result = Math.sqrt(variance);
        return result;
    }

    /**
     * Compute splope of Linear regression (only the a in y=ax+b)
     * @return
     */
    public double getSlopeOfLinearRegression() {
        double result = Double.NaN;
        try {
            double mX = (call + 1) / 2;
            double mY = total / call;
            double mX2 = (call + 1) * (2 * call + 1) / 6;
            double mXY = lrTimeCall / call;
            result = (mXY - mX * mY) / (mX2 - mX * mX);
        } catch (Exception eee) {
            // probably call = 0 -> division by 0
        }
        return result;
    }

    /**
     * You can change implementation of this method in children class
     * (ex: convert to second for time, or 'Mo' for memory)
     * @param value
     * @return
     */
    public String formatValue(long value) {
        String result = String.valueOf(value);
        return result;
    }

    public StringBuilder exportCSV(StringBuilder result) {
        if (result == null) {
            result = new StringBuilder();
        }
        result.append(getName()).append(";")
                .append(call).append(";")
                .append(formatValue(min)).append(";")
                .append(formatValue((long) getMean())).append(";")
                .append(formatValue(max)).append(";")
                .append(formatValue(total)).append(";")
                .append(formatValue((long) getStandardDeviation()))
                .append(";").append(getSlopeOfLinearRegression());
        return result;
    }

    public StringBuilder exportJSON(StringBuilder result) {
        if (result == null) {
            result = new StringBuilder();
        }
        result.append("'").append(getName()).append("': {");
        result.append("'name':").append("'").append(getName()).append("'").append(",")
                .append("'call':").append(call).append(",")
                .append("'min':").append(formatValue(min)).append(",")
                .append("'mean':").append(formatValue((long) getMean())).append(",")
                .append("'max':").append(formatValue(max)).append(",")
                .append("'total':").append(formatValue(total)).append(",")
                .append("'deviation':").append(formatValue((long) getStandardDeviation())).append(",")
                .append("'slope':").append(getSlopeOfLinearRegression());
        result.append("}");
        return result;
    }

    public StringBuilder exportText(StringBuilder result) {
        if (result == null) {
            result = new StringBuilder();
        }
        result.append(getName()).append("\t")
                .append(" call: ").append(call)
                .append(" min: ").append(formatValue(min))
                .append(" mean: ").append(formatValue((long) getMean()))
                .append(" max: ").append(formatValue(max))
                .append(" total: ").append(formatValue(total))
                .append(" deviation: ").append(getStandardDeviation())
                .append(" slope: ").append(getSlopeOfLinearRegression());
        return result;
    }

}
