/*
 * #%L
 * Nuiton Profiling
 * %%
 * Copyright (C) 2004 - 2013 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
jQuery(document).ready(function($) {
    restoreDatabase();

    // each 10s update memory and trace statistics
    setTimeout(CallUpdateMemory, 10000);

    $('#profilingUrl').val(window.location.origin);

    $('#addStat').click(function () {
        requestNewData();
    });

    $('#clearStat').click(function () {
        var url = $("#profilingUrl").val() + "/api/clear";
        $.get(url);
        addMenuEntry();
    });

    $('#deleteAllStat').click(function () {
        if (confirm('Do you really remove all stored data ?')) {
            getMenu().empty();
            saveDatabase();
        }
    });

    $('#download').click(function () {
        var content = '';

        var fields = ['thread', 'method', 'call', 'time', 'timewithnest', 'min', 'mean', 'max', 'deviation', 'slope'];
        content += fields.join(';') + '\n';
        var rows =  getGrid().jqGrid().getRowData();
        $.each( rows, function(i, info){
            var sep = '';
            for (var i=0; i<fields.length; i++) {
                if (i === 0) {
                    content += sep + $(info[fields[i]]).text();
                } else {
                    content += sep + info[fields[i]];
                }
                sep = ';';
            }
            content += '\n';
        });

        window.URL = window.URL || window.webkitURL;
        var a = document.createElement('a');
        var blob = new Blob([content], {'type':'text/csv'});
        a.href = window.URL.createObjectURL(blob);
        a.download = 'nuitonprofiling.csv';
        a.click();
        window.setTimeout(function() {
            // on delai car sinon, le click a pas le temps de se faire
            window.URL.revokeObjectURL(a.href);
        }, 1000);
    });

    getGrid().jqGrid({
        datatype: processrequest,
        mtype: 'POST',
        jsonReader : {
            root:"rows",
            page: "page",
            total: "total",
            records: "records",
            repeatitems: false,
            id: "idx"
        },
        colNames:["idx", "thread", "method","call","time", "timewithnest", "min", "mean", "max", 'deviation', 'slope'],
        colModel:[
            {name:'idx', index:'idx', hidden: true, key:true, firstsortorder:'desc'},
            {name:'thread', index:'thread', width:40, align: "left"},
            {name:'method', index:'method', align: "left", formatter:methodFormatter},
            {name:'call', index:'call', width:25, align:"right", sorttype:'float', firstsortorder:'desc'},
            {name:'time', index:'time', width:25, align:"right", formatter:timeFormatter, sorttype:'float', firstsortorder:'desc'},
            {name:'timewithnest', index:'timewithnest', width:25, align:"right", formatter:timeFormatter, sorttype:'float', firstsortorder:'desc'},
            {name:'min', index:'min', width:25, align:"right", sorttype:'float', formatter:timeFormatter, firstsortorder:'desc'},
            {name:'mean', index:'mean', width:25, align:"right", sorttype:'float', formatter:timeFormatter, firstsortorder:'desc'},
            {name:'max', index:'max', width:25 ,align:"right", sorttype:'float', formatter:timeFormatter, firstsortorder:'desc'},
            {name:'deviation', index:'deviation', width:25 ,align:"right", sorttype:'float', formatter:timeFormatter, firstsortorder:'desc'},
            {name:'slope', index:'slope', width:25 ,align:"right", sorttype:'float', formatter:floatFormatter, firstsortorder:'desc'}
        ],
        height:'auto',
        autowidth: true,
        forceFit: true,
        //    rownumbers: true,
        //    treeGrid: true,
        sortname: 'time',
        sortorder: 'desc',
        treeGridModel: 'adjacency',
        ExpandColumn : 'method',
        caption: "Info"
    });

    getGrid().jqGrid('filterToolbar', {searchOnEnter : false});

});


function requestNewData() {
    var url = $("#profilingUrl").val() + "/data/json";
    getNewData(url);
}

function getGrid() {
    return $("#treegrid");
}

function getInfo() {
    return $("#info");
}

var currentData = {};

/**
 * Charge les data comme current data et force le rechargement de la grille
 */
function loadData(data) {
    currentData = data;
    updateInfo(currentData);
    createGraph(currentData);
    getGrid().trigger("reloadGrid");
}

function updateInfo(data) {
    var text = "load " + Object.getOwnPropertyNames(data).length + " methods statistics";
    getInfo().text(text);
}

function CallUpdateMemory() {
    var url = $("#profilingUrl").val() + "/data/json";
    getNewData(url + "/memory", updateMemory);

    // each 10s update memory statistics
    setTimeout(CallUpdateMemory, 10000);
}

function updateMemory(data) {
    var span = $("#memory");
    var d = data.memory;
    var text = "Memory mean used: " + d.mean + "Mo max: " + d.max + "Mo deviation: " + d.deviation + " slope: " + d.slope;
    span.text(text);
}

/**
 * Recupere les donnees d'une instance de Nuiton Profiling et l'ajoute
 * a la liste des donnees disponibles
 */
function getNewData (url, callback) {
    callback = callback || addMenuEntry;
    $.ajax({
        type: "GET",
        url: url,
        dataType: "jsonp",
        success: function(data){
            callback(data);
        }
    });
}

/**
 * Creer un lien dans le menu
 */
function createLinkToLoad(text) {
    var result = $('<a href="#">'+text+'</a>').click(function() {
        loadData($(this).parent().data('profiling-data'));
    });
    return result;
}

/**
 * retourne le menu des snapshot disponible
 */
function getMenu() {
    var menu = $("#datas");
    return menu;
}

/**
 * sauvegarde dans la base local pour pouvoir recuperer les info entre deux sessions
 */
function saveDatabase() {
    var db = [];
    getMenu().children().each(function (i, e) {
       var data = $(this).data('profiling-data');
       var text = $(this).text();
       db.push({text:text, data:data});
    });
    var json = JSON.stringify(db);
    localStorage.setItem('profiling', json);
}

/**
 * recuperer les info sauvegardees
 */
function restoreDatabase() {
    var json = localStorage.getItem('profiling') || '[]';
    var db = JSON.parse(json);
    $.each(db, function(i, e) {
        addMenuEntry(e.data, e.text, true);
    });
}

/**
 * Ajoute une entre au menu des snapshot disponible et fait un appel a la
 * sauvegarde les donnees dans la base
 *
 * // TODO retravailler le code, il est vraiment crado :(
 */
function addMenuEntry(data, title, restoration) {
    var menu = getMenu();
    var li;
    if (data) {
        var date = title || new Date();

        var a = createLinkToLoad(date);
        var edit = $('<li class="glyphicon glyphicon-pencil glyphicon-large blue"></li>').click(function() {
            var a = $(this).parent().children("a");
            var text = a.text();

            var input = $('<input type="text" value="'+text+'"></input>')
            .click(function () {
                return false; // false pour que le menu, ne se referme pas
            })
            .keyup(function (e) {
                if (e.keyCode == 13) {
                    // retour chariot == validation
                    var text = $(this).val();
                    var a = createLinkToLoad(text);
                    $(this).replaceWith(a);
                    saveDatabase();
                } else if (e.keyCode == 27) {
                    // ESC == abandont
                    var text = $(this).data('old-text');
                    var a = createLinkToLoad(text);
                    $(this).replaceWith(a);
                }
            })
            .blur(function () {
                // on perd le focus == validation
                var text = $(this).val();
                var a = createLinkToLoad(text);
                $(this).replaceWith(a);
                saveDatabase();
            });
            input.data('old-text', text);
            a.replaceWith(input);
            input.focus();
            input.select();
            return false; // false pour que le menu, ne se referme pas
        });
        var remove = $('<li class="glyphicon glyphicon-trash glyphicon-large red"></li>').click(function() {
            $(this).parent().remove();
            saveDatabase();
        });
        li = $('<li class="inline"></li>');
        li.data("profiling-data", data);

        li.append(a);
        li.append(edit);
        li.append(remove);
    } else {
        li = $('<li class="divider"></li>');
    }
    menu.append(li);
    restoration || saveDatabase();
}


/**
 * Pour les arbres les parametres sont:
 * nodeid: le nom du noeud qui a ete ouvert
 * n_level: le level du noeud qui a ete ouvert
 * parentid: le noeud pere
 *
 * Les autres parametres:
 * _search=false
 * nd=1358630099165
 * rows=10000
 * page=1
 * sidx=
 * sord=asc
 */
function processrequest(args) {
//    var p = "";
//    for (var f in args) {
//        p += " "+f+"="+args[f];
//    }
//    console.log("r: '" + p + "'");
    var thegrid = getGrid()[0];
    
    var jsonObject = createGridData(currentData, args, args.nodeid, args.n_level, args.sidx, args.sord);

    thegrid.addJSONData(jsonObject);
}

function checkFilter(filter, row) {
    var result =
        (!filter.thread || row.thread.indexOf(filter.thread) > 0) &&
        (!filter.method || row.method.indexOf(filter.method) > 0) &&
        (!filter.call || row.call - filter.call > 0) &&
        (!filter.time || row.time - filter.time > 0) &&
        (!filter.timewithnest || row.timewithnest - filter.timewithnest > 0) &&
        (!filter.min || row.min - filter.min > 0) &&
        (!filter.mean || row.mean - filter.mean > 0) &&
        (!filter.max || row.max - filter.max > 0) &&
        (!filter.deviation || row.deviation - filter.deviation > 0) &&
        (!filter.slope || row.slope - filter.slope > 0)
        ;
    return result;
}

function createGridData(data, filter, parent, level, sortField, sortOrder) {
    if (level === undefined) {
        level = 0;
    } else {
        level++;
    }
    var rows = [];
    if (data) {
        $.each( data, function(method, info){
            var row = {};
            row.idx = level + '_' + rows.length;
            row.level = level;
            row.isLeaf = false;
            row.parent = parent;

            row.thread = info.thread;
            row.method = info.method;
            row.call = info.call;
            row.time = info.total;
            row.timewithnest = info.total_with_nest;
            row.min = info.min;
            row.mean = info.mean;
            row.max = info.max;
            row.deviation = info.deviation;
            row.slope = info.slope;

            if (checkFilter(filter, row)) {
                rows.push(row);
            }
        } );
    }

    sortRow(rows, sortField, sortOrder);

    var result = {
        "total": rows.length,
        "page": "1",
        "records": rows.length,
        "rows" : rows
    };

    return result;
}

function sortRow(rows, field, order) {
    if (field === 'method') {
        rows.sort();
     } else {
         rows.sort(function(a,b) {
             var result = a[field] - b[field];
             return result;
         });
     }
     if (order === 'desc') {
         rows.reverse();
     }
}

function methodFormatter (cellvalue, options, rowObject) {
   var shortName = cellvalue.replace(/[^\(]*\.([_\w\$]+\.[_\w\$]+).*/, "$1");
   var result = "<span title='" + cellvalue + "'>" + shortName + "</span>";
   return result;
}

function two(x) {return ((x>9)?"":"0")+x;}
function three(x) {return ((x>99)?"":"0")+((x>9)?"":"0")+x;}

function floatFormatter (cellvalue, options, rowObject) {
    return cellvalue && cellvalue.toFixed(2) || "NaN";
}

function timeFormatter (cellvalue, options, rowObject) {
    //    return cellvalue;
    var ms = cellvalue * 1000;
    var sec = Math.floor(ms/1000);
    ms = ms % 1000;
    t = three(ms);

    var min = Math.floor(sec/60);
    sec = sec % 60;
    t = two(sec) + "." + t;

    var hr = Math.floor(min/60);
    min = min % 60;
    t = two(min) + ":" + t;

    if (hr > 0) {
        t = two(hr) + ":" + t;
    }

    return t;
}

function createGraph(data) {
    var callSum = 0;
    var timeSum = 0;
    var timeWithNestSum = 0;
    var call = [];
    var time = [];
    var timeWithNest = [];
    jQuery.each( data, function(method, info){

        var shortName = method.replace(/[^\(]*\.([_\w\$]+\.[_\w\$]+).*/, "$1");

        call.push([shortName, info.call]);
        time.push([shortName, info.total]);
        timeWithNest.push([shortName, info.total_with_nest]);

        callSum += info.call;
        timeSum += info.total;
        timeWithNestSum += info.total_with_nest;
    });

    graph('Call','graphCall', call, callSum);
    graph('Time','graphTime', time, timeSum);
    graph('Time with nest','graphTimeWithNest', timeWithNest, timeWithNestSum);
}

/**
 * @param name titre du graph
 * @param id l'identifiant de l'element HTML a utiliser pour mettre le graph
 * @param data les donnees de type: [ ['Heavy Industry', 12],['Retail', 9], ...]
 * @param total le total de la somme des donnees
 */
function graph(name, id, data, total){
    sortRow(data, 1, "desc");

    data = data.slice(0, 5);

    var others = total;
    jQuery.each( data, function(i, info){
        var val = info[1];
        others -= val;
        info[1] = val;
    });

    data.push(['others', others]);

    var plot = jQuery.jqplot (id, [data], {
        title: name,
        seriesDefaults: {
            // Make this a pie chart.
            renderer: jQuery.jqplot.PieRenderer,
            rendererOptions: {
                diameter: '200',
                showDataLabels: true,
                sliceMargin: 5,
                startAngle: 180
            }
        },
        highlighter: {
            show: true,
            formatString:'%s',
            tooltipLocation:'se',
            useAxesFormatters:false
        },
        legend: {show:false, location: 's'}
    });
}
