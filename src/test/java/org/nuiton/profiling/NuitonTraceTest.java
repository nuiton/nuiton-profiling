/*
 * #%L
 * Nuiton Profiling
 * %%
 * Copyright (C) 2004 - 2014 CodeLutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.profiling;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.Assert;
import org.junit.Test;

/**
 * Launch this test with -javaagent:target/lib/aspectjweaver-1.7.1.jar
 *
 * @author tchemit &lt;chemit@codelutin.com&gt;
 * @since 2.3
 */
public class NuitonTraceTest {

    @Test
    public void testAOP() {
        Log log = LogFactory.getLog(NuitonTraceTest.class);

        if (log.isDebugEnabled()) {
            log.debug("DEBUG");
        }
        if (log.isInfoEnabled()) {
            log.info("INFO");
        }

        if (log.isErrorEnabled()) {
            log.error("ERROR");
        }

        if (log.isWarnEnabled()) {
            log.warn("WARN");
        }

        if (log.isTraceEnabled()) {
            log.trace("TRACE");
        }

        // trace should have something inside it...
        String statisticsAndClear = NuitonTrace.getStatisticsAndClear();
        Assert.assertTrue(StringUtils.isNotEmpty(statisticsAndClear));

        if (log.isInfoEnabled()) {
            log.info(statisticsAndClear);
        }
    }

    // pour pouvoir tester la meme chose avec un simple java -javaagent:...
    // par exemple:
    // java -javaagent:target/lib/aspectjweaver-1.7.1.jar -classpath target/classes:target/lib/aspectjrt-1.7.1.jar:target/lib/aspectjweaver-1.7.1.jar:target/lib/commons-lang-2.6.jar:target/lib/commons-logging-1.1.1.jar:target/lib/junit-4.8.2.jar:target/lib/log4j-1.2.16.jar:target/test-classes org.nuiton.profiling.NuitonTraceTest

    public static void main(String... args) {
        NuitonTraceTest t = new NuitonTraceTest();
        t.testAOP();
    }

}
