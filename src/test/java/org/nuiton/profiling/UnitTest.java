package org.nuiton.profiling;

/*
 * #%L
 * Nuiton Profiling
 * %%
 * Copyright (C) 2014 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */


import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.Assert;
import org.junit.Test;

/**
 *
 * @author poussin
 * @version $Revision$
 *
 * Last update: $Date$
 * by : $Author$
 */
public class UnitTest {

    /** to use log facility, just put in your code: log.info(\"...\"); */
    static private Log log = LogFactory.getLog(UnitTest.class);

    @Test
    public void testFormatTime() throws Exception {
        Assert.assertEquals("12min -> nano: ", String.format("%,.3fnano", 720000000000.000), Unit.format(12, Unit.Time.m, Unit.Time.nano, 3));
        Assert.assertEquals("1milli -> s: ", String.format("%,.3fs", 0.001), Unit.format(1, Unit.Time.milli, Unit.Time.s, 3));
        Assert.assertEquals("1day -> m: ", String.format("%,.3fm", 1440.000), Unit.format(1, Unit.Time.d, Unit.Time.m, 3));
    }

    @Test
    public void testFormatMemory() throws Exception {
        Assert.assertEquals("4,5Go -> o: ", String.format("%,.3fo", 4831838208.000), Unit.format(4.5, Unit.Memory.Go, Unit.Memory.o, 3));
        Assert.assertEquals("4o -> ko: ", String.format("%,.3fKo", 0.004), Unit.format(4, Unit.Memory.o, Unit.Memory.Ko, 3));
        Assert.assertEquals("5400o -> Mo: ", String.format("%,.3fMo", 0.005), Unit.format(5400, Unit.Memory.o, Unit.Memory.Mo, 3));
    }

    @Test
    public void testBestFormatTime() throws Exception {
        Assert.assertEquals("120m: ", String.format("%,.3fh", 2.000), Unit.format(120, Unit.Time.m, 3));
        Assert.assertEquals("0,001s: ", String.format("%,.3fmilli", 1.000), Unit.format(0.001, Unit.Time.s, 3));
        Assert.assertEquals("1day: ", String.format("%,.3fd", 1.000), Unit.format(1, Unit.Time.d, 3));
        Assert.assertEquals("0,001nano: ", String.format("%,.3fnano", 0.001), Unit.format(0.001, Unit.Time.nano, 3));
    }

    @Test
    public void testBestFormatMemory() throws Exception {
        Assert.assertEquals("4831838208", String.format("%,.3fGo", 4.500), Unit.format(4831838208L, Unit.Memory.o, 3));
        Assert.assertEquals("0,004Ko", String.format("%,.0fo", 4.0), Unit.format(0.004, Unit.Memory.Ko, 0));
        Assert.assertEquals("5400o", String.format("%,.1fKo", 5.3), Unit.format(5400, Unit.Memory.o, 1));
        Assert.assertEquals("3000To", String.format("%,.1fTo", 3000.0), Unit.format(3000, Unit.Memory.To, 1));
    }

}
